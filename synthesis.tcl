read_vhdl -vhdl2008 ./components/arithmetic_units.vhd
read_vhdl -vhdl2008 ./components/delay_buffer.vhd
read_vhdl -vhdl2008 ./components/elastic_components.vhd
read_vhdl -vhdl2008 ./components/MemCont.vhd
read_vhdl -vhdl2008 ./components/multipliers.vhd
read_vhdl -vhdl2008 ./components/mul_wrapper.vhd
read_vhdl -vhdl2008 ./components/sharing_components.vhd

set bitWidth 32

# elasticBuffer (1OB + 1TB)
set unitName elasticBuffer

synth_design -top $unitName \
	-generic INPUTS=1 \
	-generic OUTPUTS=1 \
	-generic DATA_SIZE_IN=$bitWidth \
	-generic DATA_SIZE_OUT=$bitWidth \
	-part xc7z020clg484-1 -no_iobuf -mode out_of_context
report_utilization -file post_synth_util_${unitName}_${bitWidth}b.rpt

# TEHB
set unitName TEHB

synth_design -top $unitName \
	-generic INPUTS=1 \
	-generic OUTPUTS=1 \
	-generic DATA_SIZE_IN=$bitWidth \
	-generic DATA_SIZE_OUT=$bitWidth \
	-part xc7z020clg484-1 -no_iobuf -mode out_of_context
report_utilization -file post_synth_util_${unitName}_${bitWidth}bit.rpt

# OEHB
set unitName OEHB

synth_design -top $unitName \
	-generic INPUTS=1 \
	-generic OUTPUTS=1 \
	-generic DATA_SIZE_IN=$bitWidth \
	-generic DATA_SIZE_OUT=$bitWidth \
	-part xc7z020clg484-1 -no_iobuf -mode out_of_context
report_utilization -file post_synth_util_${unitName}_${bitWidth}bit.rpt

# Transparent FIFO (fully transparent buffer)
set unitName transpFifo
set numSlots 4

synth_design -top $unitName \
	-generic INPUTS=1 \
	-generic OUTPUTS=1 \
	-generic DATA_SIZE_IN=$bitWidth \
	-generic DATA_SIZE_OUT=$bitWidth \
	-generic FIFO_DEPTH=$numSlots \
	-part xc7z020clg484-1 -no_iobuf -mode out_of_context
report_utilization -file post_synth_util_${unitName}_${bitWidth}bit_${numSlots}slots.rpt

# Non transparent FIFO (equivalent to 1 OB + (N-1) FT + 1 TB)
set unitName nontranspFifo
set numSlots 4

synth_design -top $unitName \
	-generic INPUT_COUNT=1 \
	-generic OUTPUT_COUNT=1 \
	-generic DATA_SIZE_IN=$bitWidth \
	-generic DATA_SIZE_OUT=$bitWidth \
	-generic FIFO_DEPTH=$numSlots \
	-part xc7z020clg484-1 -no_iobuf -mode out_of_context
report_utilization -file post_synth_util_${unitName}_${bitWidth}bit_${numSlots}slots.rpt
