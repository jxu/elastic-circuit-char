# Synthesize dataflow units in isolation

## Usage:

```sh
$ vivado-2019.1.1 vivado -mode batch -source synthesis.tcl
```

Which saves the report in `post_synth_util_*.rpt`.

## Changing the unit parameters:

Changing the buffer's bitwidth:

```diff
--- set bitWidth 32
+++ set bitWidth 12 # or whatever you need
```

Changing the buffer's number of slots:

```diff
--- set numSlots 4
+++ set numSlots 12 # or whatever you need
```

## Reading the report:

``` 
1. Slice Logic
--------------

+-------------------------+------+-------+-----------+-------+
|        Site Type        | Used | Fixed | Available | Util% |
+-------------------------+------+-------+-----------+-------+
| Slice LUTs*             |   20 |     0 |     53200 |  0.04 |
|   LUT as Logic          |   20 |     0 |     53200 |  0.04 |
|   LUT as Memory         |    0 |     0 |     17400 |  0.00 |
| Slice Registers         |   66 |     0 |    106400 |  0.06 |
|   Register as Flip Flop |   66 |     0 |    106400 |  0.06 |
|   Register as Latch     |    0 |     0 |    106400 |  0.00 |
| F7 Muxes                |    0 |     0 |     26600 |  0.00 |
| F8 Muxes                |    0 |     0 |     13300 |  0.00 |
+-------------------------+------+-------+-----------+-------+
```
